<?php

use Illuminate\Database\Seeder;
use App\Airports;

class AirportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Airports::truncate();

	Airports::create([
		'code' => 'YUL',
		'city_code' => 'YMQ',
		'name' => 'Pierre Elliot Trudeau International',
		'city' => 'Montreal',
		'country_code' => 'CA',
		'region_code' => 'QC',
		'latitude' => '45.457714',
		'longitude' => '-73.749908',
		'timezone' => 'America/Montreal'
	]);

	 Airports::create([
                'code' => 'YVR',
                'city_code' => 'YVR',
                'name' => 'Vancouver International',
                'city' => 'Vancouver',
                'country_code' => 'CA',
                'region_code' => 'BC',
                'latitude' => '49.194698',
                'longitude' => '-123.179192',
                'timezone' => 'America/Vancouver'
        ]);

    }
}
