<?php

use Illuminate\Database\Seeder;
use App\Airlines;

class AirlinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Airlines::truncate();


	Airlines::create([
		'code' => 'AC',
		'name' => 'Air Canada'
	]);
    }
}
