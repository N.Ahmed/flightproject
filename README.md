# FlightProject


This is a locally hosted FlightProject with created in the Laravel framework.

The project will interact with the server side API to get and return flight information for the corresponding destination as indicated by
the user.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

These prerequisites are required to run the server itself and was only tested in the Ubuntu distribution

Mysql 


Ubuntu

```
sudo apt-get update
sudo apt-get install mysql-server

```
you also need to enable the service for more information please refer to this guide: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-16-04

PHP (7.0 or Higher)

```
sudo apt-get update
sudo apt-get install php7.0-common

```

Composer

Full Guide on how to install Composer: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-16-04


### Installing


The first step is to make a copy of the env.example file which is located at the root of the directory. This file will handle the Database request of logging in and accessing certain fields.

ex In Linux's command line:

```
cp .env.example .env

```

In this file you'll see 3 fields

```
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

```

This is here to interact with your MySQL database which can store the API data within it. Enter your username, password, and database
name that you want this project to interact with.


Before you begin migrating the tables from the project to the database, run these commands on the Linux command line

```
composer update --no-scripts  
php artisan key:generate
```

This is to rebuild the missing libraries that were ignored while version controlling through git. 

NOTE: php artisan key:generate requires you have a .env file set up before it can correctly be called


If the .env file is configured correctly and running the two above commands, you may begin migrating the tables from the project to your mysql database. Run this command to migrate the tables from the project to the database:

```
php artisan migrate
``` 

This creates the tables in the database to fill it with the values run the following command:

```
php artisan db:seed
```


This will enable you to see the data and finally you want to start hosting locally which can by done executing:

```

php artisan serve

```

You will now now host the project on the local host and be able to access the home page. If you would like to see the api page after hosting locally you may see them as by writing the following:

```
http://127.0.0.1:8000/api/flights
http://127.0.0.1:8000/api/airlines
http://127.0.0.1:8000/api/airports
```

## Built With

Laravel - PHP Framework

## Author

Nabid Ahmed

