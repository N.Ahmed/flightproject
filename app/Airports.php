<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airports extends Model
{
    protected $fillable = ['code', 'city_cody', 'name', 'city', 'country_code', 'region_code', 'latitude', 'longitude', 'timezone'];
}
