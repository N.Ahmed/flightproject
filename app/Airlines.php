<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airlines extends Model
{
    protected $fillable = ['code', 'name'];
}
