<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flights extends Model
{
	protected $fillable = ['airline','number','departure_airport','departure_time','arrival_airport','arrival_time','price'];
}
