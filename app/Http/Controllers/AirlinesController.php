<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airlines;

class AirlinesController extends Controller
{
    	/**
	 * Index function for general listing.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		$airlines = Airlines::all();
 
		return response()->json($airlines);
	}
 
 
	/**
	 * Store-Action
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		$airlines = Airlines::create($request->all());
 
		return response()->json($airlines);
	}
 
 
	/**
	 * Show-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show(Request $request, $id)
	{
		$airlines = Airlines::find($id);
 
		return response()->json($airlines);
	}
 
 
	/**
	 * Update-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, $id)
	{
		$airlines = Airlines::findOrFail($id);
		$airlines->update($request->all());
 
		return response()->json($airlines);
 
 
	}
 
 
	/**
	 * Destroy-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroy(Request $request, $id)
	{
		Airlines::find($id)->delete();
 
		return response()->json([], 204);
	}
}
