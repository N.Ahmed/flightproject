<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airports;

class AirportsController extends Controller
{
	/**
	 * Index function for general listing.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		$airports = Airports::all();
 
		return response()->json($airports);
	}
 
 
	/**
	 * Store-Action
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		$airports = Airports::create($request->all());
 
		return response()->json($airports);
	}
 
 
	/**
	 * Show-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show(Request $request, $id)
	{
		$airports = Airports::find($id);
 
		return response()->json($airports);
	}
 
 
	/**
	 * Update-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, $id)
	{
		$airports = Airports::findOrFail($id);
		$airports->update($request->all());
 
		return response()->json($airports);
 
 
	}
 
 
	/**
	 * Destroy-Action
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroy(Request $request, $id)
	{
		Airports::find($id)->delete();
 
		return response()->json([], 204);
	}
}
