<?php

namespace App\Http\Controllers;

use App\Flights;
use Illuminate\Http\Request;

class FlightsController extends Controller
{
    public function index()
    {
        return Flights::all();
    }

    public function show(Flights $flights) {
        return $flights;
    }

    public function store(Request $request){
        $flights =  Flights::create($request->all());
        return response()->json($flights, 201);
    }

    public function update(Request $request, Flights $flights) {
        $flights->update($request->all());

        return response()->json($flights, 200);
    }

    public function delete(Flights $flights)
    {
        $flights->delete();

        return response()->json(null, 204);
    }
}
