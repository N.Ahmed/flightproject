<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Awesome Tickets Deals</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .travel-form {
                width: 300px;
                border: 25px solid blue;
                padding: 25px;
                margin: 25px;
            }

            .btn-group button {
    background-color: #4CAF50; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: center; /* Float the buttons side by side */
}

.btn-group button:focus {
    background-color: blue; /* Green background */
    border: 1px solid blue; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: center; /* Float the buttons side by side */
}

.btn-group button:not(:last-child) {
    border-right: none; /* Prevent double borders */
}

/* Clear floats (clearfix hack) */
.btn-group:after {
    content: "";
    clear: both;
    display: table;
}

/* Add a background color on hover */
.btn-group button:hover {
    background-color: #3e8e41;
}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Ticket Deals
                </div>


                
                    <div class="travel-form">
                        <form action="{{action('GetFlights@flights')}}" method="post">
                            <input id="ml-token" name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <select name="from-city">
                        <option value="Montreal">Montreal</option>
                        <option value="Vancouver">Vancouver</option>
                    </select>
                    <select name="to-city">
                        <option value="Vancouver">Vancouver</option>
                        <option value="Montreal">Montreal</option>
                    </select>
                     <div class="btn-group">
                       <input type="radio" name="flight-return" value="one"> One Way<br>
                         <input type="radio" name="flight-return" value="round"> Round Trip<br>
                    </div> 
		          <input type='submit' name='submit'/>
              </form>
              </div>
                   
            </div>
        </div>
    </body>
</html>
