<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
       <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
$( document ).ready(function() {
    $.get( "http://127.0.0.1:8000/api/airports", function( data ){
    $.each( data, function( keyed, valed ) {
    $.each( valed, function(keyss, valss){
        var from_city = "{{ request()->input('from-city') }}";
            if (valss == from_city){
             $.each( valed, function(keyss, valss){
                if (keyss == "code")
                $( ".test" ).append( valss );
             });
        }

        
    });  
    });
    });


     $.get( "http://127.0.0.1:8000/api/flights", function( data ){
                    $.each( data, function( key, val ) {
                    $.each( val, function(keys, vals){
                        var code = $( ".test" ).text();
                        var flights_return = "{{ request()->input('flight-return') }}";
                        if (flights_return == "round"){
                            if (val["departure_airport"] == code){
                          if (vals == val["airline"]){
                             $.get( "http://127.0.0.1:8000/api/airlines", function( data ){
                                    $.each( data, function( key, val ) {
                                    $.each( val, function(keys, vals){
                                        if (vals == val['code']){
                                            $.each( val, function(keys, vals){
                                                if (vals == val['name']){
                                                    $(".airlines").append(vals);
                                                }
                                            });
                                        }
                                });
                            });
                        });
                          }

                          if (vals == val["departure_airport"]){
                            $(".departure_airport").append(vals);
                          }

                          if (vals == val["departure_time"]){
                            $(".departure_time").append(vals);
                          }

                          if (vals == val["arrival_airport"]){
                            $(".arrival_time").append(vals);
                          }

                          if (vals == val["arrival_time"]){
                            $(".arrival_time").append(vals);
                          }

                          if (vals == val["price"]){
                            $(".price").append(vals);
                          }
                      } else {
                        if (vals == val["airline"]){
                             $.get( "http://127.0.0.1:8000/api/airlines", function( data ){
                                    $.each( data, function( key, val ) {
                                    $.each( val, function(keys, vals){
                                        if (vals == val['code']){
                                            $.each( val, function(keys, vals){
                                                if (vals == val['name']){
                                                    $(".airlines_return").append(vals);
                                                }
                                            });
                                        }
                                });
                            });
                        });
                          }

                          if (vals == val["departure_airport"]){
                            $(".departure_airport_return").append(vals);
                          }

                          if (vals == val["departure_time"]){
                            $(".departure_time_return").append(vals);
                          }

                          if (vals == val["arrival_airport"]){
                            $(".arrival_airport_return").append(vals);
                          }

                          if (vals == val["arrival_time"]){
                            $(".arrival_time_return").append(vals);
                          }

                          if (vals == val["price"]){
                            $(".price_return").append(vals);
                          }
                      }
                        } else if (flights_return == 'one'){
                        if (val["departure_airport"] == code){
                          if (vals == val["airline"]){
                             $.get( "http://127.0.0.1:8000/api/airlines", function( data ){
                                    $.each( data, function( key, val ) {
                                    $.each( val, function(keys, vals){
                                        if (vals == val['code']){
                                            $.each( val, function(keys, vals){
                                                if (vals == val['name']){
                                                    $(".airlines").append(vals);
                                                }
                                            });
                                        }
                                });
                            });
                        });
                          }

                          if (vals == val["departure_airport"]){
                            $(".departure_airport").append(vals);
                          }

                          if (vals == val["departure_time"]){
                            $(".departure_time").append(vals);
                          }

                          if (vals == val["arrival_airport"]){
                            $(".arrival_airport").append(vals);
                          }

                          if (vals == val["arrival_time"]){
                            $(".arrival_time").append(vals);
                          }

                          if (vals == val["price"]){
                            $(".price").append(vals);
                          }
                        }
                    }
                 });
            });
        });

    
    });

</script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: -47px;
            }

            div .ticket_info {
    background-color: lightgrey;
    width: 88%;
    border: 25px solid green;
    padding: 25px;
    margin: 25px;
}
        </style>
        

    </head>
    <body>
        <div class="flex-center position-ref full-height">
             <div class="content">
                 <div class="title m-b-md">
                    Ticket from {{ request()->input('from-city') }} to {{ request()->input('to-city') }}
                </div>
             <div class="ShowDataHere">
        @if(!empty(request()->all()))
            <div class="test" style="display: none;"></div>
            <div class="ticket_info" >
                <ul>
                    <li class='airlines'>Airlines: </li>
                    <li class='departure_airport'>Departure Airport: </li>
                    <li class="departure_time">Departure Time: </li>
                    <li class='arrival_airport'>Arrival Airport: </li>
                    <li class='arrival_time'>Arrival Time: </li>
                    <li class='price'>Price: </li>
                </ul>
                <a href="{{ url('/') }}">Return to Home Page</a>
            </div>


            
            </div>
            
        @if (strcmp(request()->input('flight-return'),"round") == 0)
        <div class="title m-b-md">
            Ticket from {{ request()->input('to-city') }} to {{ request()->input('from-city') }}
            </div>
        <div class="ticket_info" >
                <ul>
                    <li class='airlines_return'>Airlines: </li>
                    <li class='departure_airport_return'>Departure Airport: </li>
                    <li class="departure_time_return">Departure Time: </li>
                    <li class='arrival_airport_return'>Arrival Airport: </li>
                    <li class='arrival_time_return'>Arrival Time: </li>
                    <li class='price_return'>Price: </li>
                </ul>
                <a href="{{ url('/') }}">Return to Home Page</a>
            </div>
        @endif
        @endif
    </div >
        </div>
    </body>
</html>
