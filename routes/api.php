<?php

use App\Users;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('flights', 'FlightsController@index');

Route::get('flights/{flights}', 'FlightsController@show');

Route::post('flights', 'FlightsController@store');

Route::put('flights/{flights}', 'FlightsController@update');

Route::delete('flights/{flights}', 'FlightsController@delete');

Route::resource('airlines', 'AirlinesController');

Route::resource('airports', 'AirportsController');
